// Helpers/Settings.cs
using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace DreamcastNow.App.Helpers
{
    /// <summary>
    /// This is the Settings static class that can be used in your Core solution or in any
    /// of your client applications. All settings are laid out the same exact way with getters
    /// and setters. 
    /// </summary>
    public static class Settings
    {
        private static ISettings AppSettings
        {
            get
            {
                return CrossSettings.Current;
            }
        }

        #region Setting Constants

        private const string LaunchView = "launchview";
        private static readonly bool LaunchViewDefault = false;
        private const string NotificationsAvailable = "NotificationsAvailable";
        private static readonly bool NotificationsAvailableDefault = true;
        private const string NotificationsEnabled = "NotificationsEnabled";
        private static readonly bool NotificationsEnabledDefault = false;
        private const string NotificationsToken = "NotificationsToken";
        private static readonly string NotificationsTokenDefault = string.Empty;

        #endregion


        public static bool LaunchViewSetting
        {
            get
            {
                return AppSettings.GetValueOrDefault<bool>(LaunchView, LaunchViewDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<bool>(LaunchView, value);
            }
        }

        public static bool NotificationsAvailableSetting
        {
            get
            {
                return AppSettings.GetValueOrDefault<bool>(NotificationsAvailable, NotificationsAvailableDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<bool>(NotificationsAvailable, value);
            }
        }

        public static bool NotificationsEnabledSetting
        {
            get
            {
                return AppSettings.GetValueOrDefault<bool>(NotificationsEnabled, NotificationsEnabledDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<bool>(NotificationsEnabled, value);
            }
        }

        public static string NotificationsTokenSetting
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>(NotificationsToken, NotificationsTokenDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<string>(NotificationsToken, value);
            }
        }
    }
}