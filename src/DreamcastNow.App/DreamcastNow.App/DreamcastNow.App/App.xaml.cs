﻿using DreamcastNow.App.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DreamcastNow.App.Helpers;

using Xamarin.Forms;

namespace DreamcastNow.App
{
    public partial class App : Application
    {
        public NavigationPage NavigationPage { get; private set; }
        public RootPage root = new RootPage();

        public App()
        {
            // The root page of your application
            Menu menu = new Menu();
            NavigationPage = new NavigationPage(new UsersView());
            NavigationPage.SetHasNavigationBar(this, true);
            root.Master = menu;
            if (Settings.LaunchViewSetting == false)
            {
                root.Detail = new UsersView();
            }
            else
            {
                root.Detail = new GameListView();
            }
            MainPage = root;
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
