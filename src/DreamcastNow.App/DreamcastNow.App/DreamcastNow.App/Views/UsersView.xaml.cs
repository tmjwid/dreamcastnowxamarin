﻿using DreamcastNow.App.Client;
using DreamcastNow.Models;
using DreamcastNow.Models.APIModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace DreamcastNow.App.Views
{
    public partial class UsersView : ContentPage
    {
        public List<OnlineUsersApiModel> model = new List<OnlineUsersApiModel>();
        public UsersView()
        {
            InitializeComponent();
            UsersList.Refreshing += UsersList_Refreshing;
            UsersList.ItemSelected += UsersList_ItemSelected;
            Title = "Users";
            UsersList.IsVisible = false;
            LoadingGrid.IsVisible = true;
        }

        private void UsersList_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            UsersList.SelectedItem = null;
        }

        private async void UsersList_Refreshing(object sender, EventArgs e)
        {
            await GetUsers();
            UsersList.EndRefresh();
        }

        protected async override void OnAppearing()
        {
            await GetUsers();
            base.OnAppearing();
        }

        private async Task<bool> GetUsers()
        {
            model = await new UserClient().GetUsers();
            OnlineUsersLabel.Text = "Current players online: " + model.Count(c => c.Online).ToString();

            foreach (var item in model)
            {
                item.CurrentGame = item.Online ? "Currently Playing " + item.CurrentGame : !string.IsNullOrEmpty(item.CurrentGame) ? "Last Played " + item.CurrentGame : string.Empty;
            }

            UsersList.ItemsSource = model.OrderByDescending(o => o.Online);
            UsersList.IsVisible = true;
            LoadingGrid.IsVisible = false;
            return true;
        }
    }

}
