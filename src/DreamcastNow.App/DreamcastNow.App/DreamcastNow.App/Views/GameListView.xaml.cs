﻿using DreamcastNow.App.Client;
using DreamcastNow.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace DreamcastNow.App.Views
{
    public partial class GameListView : ContentPage
    {
        public List<GameModel> model = new List<GameModel>();
        public bool IsBusy { get; set; }
        public bool IsRefreshing { get; set; }

        public GameListView()
        {
            InitializeComponent();
            GamesList.Refreshing += GamesList_Refreshing;
            Title = "Games";
            GamesList.IsVisible = false;
            LoadingGrid.IsVisible = true;
        }

        private async void GamesList_Refreshing(object sender, EventArgs e)
        {
            bool success = await GetGames();
            GamesList.EndRefresh();
        }

        protected async override void OnAppearing()
        {
            //LoadingModal.IsVisible = true;
            await GetGames();
            base.OnAppearing();
        }

        private async Task<bool> GetGames()
        {
            try
            {
                model = await new GameClient().GetGames();
                GamesList.ItemsSource = model;
                GamesList.IsVisible = true;
                LoadingGrid.IsVisible = false;
                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }

        }
    }
}
