﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;

using Xamarin.Forms;

namespace DreamcastNow.App.Views
{
    public class RootPage : MasterDetailPage
    {
        public RootPage()
        {
            Title = "Menu";
            MasterBehavior = MasterBehavior.Popover;
        }
    }
}
