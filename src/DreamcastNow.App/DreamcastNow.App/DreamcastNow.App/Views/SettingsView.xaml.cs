﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DreamcastNow.App.Helpers;

using Xamarin.Forms;

namespace DreamcastNow.App.Views
{
    public partial class SettingsView : ContentPage
    {
        public SettingsView()
        {
            InitializeComponent();
            ChangeLaunchView.OnChanged += ChangeLaunchView_OnChanged;
            ChangeLaunchView.On = Settings.LaunchViewSetting;
            NotificationsHeader.Title = Settings.NotificationsAvailableSetting ? "Notifications setting" : "Notifications setting (disabled: play services not available)";
            OnlinePlayersNotificationEnabled.OnChanged += OnlinePlayersNotificationEnabled_OnChanged;
            OnlinePlayersNotificationEnabled.IsEnabled = Settings.NotificationsAvailableSetting;
            OnlinePlayersNotificationEnabled.On = Settings.NotificationsEnabledSetting;
            Title = "Settings";
        }

        private void OnlinePlayersNotificationEnabled_OnChanged(object sender, ToggledEventArgs e)
        {
            Settings.NotificationsEnabledSetting = e.Value;
            MessagingCenter.Send(this, "NotificationSubscriptionChanged", e.Value);
        }

        private void ChangeLaunchView_OnChanged(object sender, ToggledEventArgs e)
        {
            Settings.LaunchViewSetting = e.Value;
        }
    }
}
