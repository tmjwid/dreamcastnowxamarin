﻿using DreamcastNow.App.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DreamcastNow.App.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DCTalkChatView : ContentPage
    {
        public DCTalkChatView()
        {
            InitializeComponent();
        }

        protected async override void OnAppearing()
        {
            await GetChat();
            base.OnAppearing();
        }

        private async Task<bool> GetChat()
        {
            DCTalkChatList.ItemsSource = await new DCTalkChatClient().GetAll();
            DCTalkChatList.IsVisible = true;
            LoadingGrid.IsVisible = false;
            return true;
        }
    }
}
