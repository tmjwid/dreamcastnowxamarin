﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;

using Xamarin.Forms;

namespace DreamcastNow.App.Views
{
    public class Menu : ContentPage
    {
        Button gamesView;
        Button settingView;
        Button usersView;
        Button scheduleView;
        Button dcTalkChat;


        public Menu()
        {
            Title = "Menu";
            gamesView = new Button() { Text = "Games", BackgroundColor = Color.Blue, TextColor = Color.White };
            gamesView.Clicked += Games_Clicked;
            settingView = new Button() { Text = "Setting", BackgroundColor = Color.Blue, TextColor = Color.White };
            settingView.Clicked += SettingView_Clicked;
            usersView = new Button() { Text = "Users", BackgroundColor = Color.Blue, TextColor = Color.White };
            settingView.VerticalOptions = LayoutOptions.End;
            scheduleView = new Button() { Text = "Game Schedules", BackgroundColor = Color.Blue, TextColor = Color.White };
            scheduleView.Clicked += ScheduleView_Clicked;
            dcTalkChat = new Button() { Text = "Chat", BackgroundColor = Color.Blue, TextColor = Color.White };
            dcTalkChat.Clicked += DcTalkChat_Clicked;
            usersView.Clicked += UsersView_Clicked;
            Content = new StackLayout
            {
                BackgroundColor = Color.White,
                Children = {
                    dcTalkChat,
                    gamesView,
                    scheduleView,
                    usersView,
                    settingView
                }
            };
            Padding = new Thickness(0, 20, 0, 0);
        }

        private void DcTalkChat_Clicked(object sender, EventArgs e)
        {
            var app = Application.Current as App;
            app.root.Detail = new DCTalkChatView();
            app.root.IsPresented = false;
        }

        private void ScheduleView_Clicked(object sender, EventArgs e)
        {
            var app = Application.Current as App;
            app.root.Detail = new ScheduleView();
            app.root.IsPresented = false;
        }

        private void UsersView_Clicked(object sender, EventArgs e)
        {
            var app = Application.Current as App;
            app.root.Detail = new UsersView();
            app.root.IsPresented = false;
        }

        private void SettingView_Clicked(object sender, EventArgs e)
        {
            var app = Application.Current as App;
            app.root.Detail = new SettingsView();
            app.root.IsPresented = false;
        }

        private void Games_Clicked(object sender, EventArgs e)
        {
            var app = Application.Current as App;
            app.root.Detail = new GameListView();
            app.root.IsPresented = false;
        }

        ~Menu()
        {
            gamesView.Clicked -= Games_Clicked;
            settingView.Clicked -= Games_Clicked;
            usersView.Clicked -= Games_Clicked;
            scheduleView.Clicked -= ScheduleView_Clicked;
        }
    }
}
