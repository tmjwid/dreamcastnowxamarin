﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DreamcastNow.App.Client
{
    public static class BaseClient
    {
#if DEBUG
            public static string BaseURL { get { return "http://localhost:4433/api/"; } }
#endif

#if RELEASE
            public static string BaseURL { get { return "https://dreamcastnow.mandark.org/api/"; } }
#endif

#if TEST
        public static string BaseURL { get { return "https://dreamcastnowtest.mandark.org/api/"; } }
#endif
    }
}
