﻿using DreamcastNow.Models;
using DreamcastNow.Models.APIModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DreamcastNow.App.Client
{
    public class UserClient 
    {
        public async Task<List<OnlineUsersApiModel>> GetUsers()
        {
            HttpClient client = new HttpClient();
            var download = await client.GetAsync(BaseClient.BaseURL + "/OnlineUsers");
            return JsonConvert.DeserializeObject<List<OnlineUsersApiModel>>(await download.Content.ReadAsStringAsync());
        }
    }
}
