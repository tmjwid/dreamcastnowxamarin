﻿using DreamcastNow.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace DreamcastNow.App.Client
{
    public class LookingForGameClient 
    {
        public async Task<List<LookingForGameModel>> GetLFG()
        {
            HttpClient client = new HttpClient();
            var download = await client.GetAsync(BaseClient.BaseURL + "/game");
            return JsonConvert.DeserializeObject<List<LookingForGameModel>>(await download.Content.ReadAsStringAsync());
        }
    }
}
