﻿using DreamcastNow.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DreamcastNow.App.Client
{
    public class GameClient
    { 
        public async Task<List<GameModel>> GetGames()
        {
            HttpClient client = new HttpClient();
            var download = await client.GetAsync(BaseClient.BaseURL + "/game");
            return JsonConvert.DeserializeObject<List<GameModel>>(await download.Content.ReadAsStringAsync());
        }
    }
}
