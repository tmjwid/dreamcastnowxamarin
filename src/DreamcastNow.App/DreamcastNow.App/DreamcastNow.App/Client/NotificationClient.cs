﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DreamcastNow.App.Client
{
    public class NotificationClient
    {
        public async Task<bool> SendNotificationToken(string token)
        {
            HttpClient client = new HttpClient();
            var content = new StringContent(new { Token = token }.ToString(), Encoding.UTF8, "application/json");
            var response = await client.PostAsync(BaseClient.BaseURL + "/Notification", content);
            return response.IsSuccessStatusCode;
        }

        public static async Task<bool> SendNotificationTokenStatic(string token, bool enabled, string oldToken)
        {
            try
            {
                HttpClient client = new HttpClient();
                var content = new StringContent(JsonConvert.SerializeObject(new { Token = token, Enabled = enabled, OldToken = oldToken }).ToString(), Encoding.UTF8, "application/json");
                var response = await client.PostAsync(BaseClient.BaseURL + "/Notification", content);
                return response.IsSuccessStatusCode;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
