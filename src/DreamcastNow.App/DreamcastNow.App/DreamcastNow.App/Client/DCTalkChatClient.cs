﻿using DreamcastNow.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DreamcastNow.App.Client
{
    public class DCTalkChatClient
    {
        public async Task<List<DCTalkChatModel>> GetAll()
        {
            HttpClient client = new HttpClient();
            var download = await client.GetAsync("http://www.dreamcast-talk.com/forum/app.php/ajaxshoutbox/getAll");
            List<DCTalkChatModel> messages = JsonConvert.DeserializeObject<List<DCTalkChatModel>>(await download.Content.ReadAsStringAsync());
            return ParseMessages(messages);
        }

        public async Task<List<DCTalkChatModel>> GetBefore(string beforeID)
        {
            HttpClient client = new HttpClient();
            var download = await client.GetAsync("http://www.dreamcast-talk.com/forum/app.php/ajaxshoutbox/getBefore/" + beforeID);
            List < DCTalkChatModel > messages = JsonConvert.DeserializeObject<List<DCTalkChatModel>>(await download.Content.ReadAsStringAsync());
            return ParseMessages(messages);
        }

        private List<DCTalkChatModel> ParseMessages(List<DCTalkChatModel> messages)
        {
            List<DCTalkChatModel> newMessages = new List<DCTalkChatModel>();
            foreach (var item in messages)
            {
                newMessages.Add(new DCTalkChatModel()
                {
                        date = item.date,
                        id = item.id, 
                        message = item.message,
                        user = item.user.Substring(item.user.IndexOf(">") + 1, item.user.IndexOf("</a>") - item.user.IndexOf(">") - 1)
                });
            }
            return newMessages;
        }
    }
}
