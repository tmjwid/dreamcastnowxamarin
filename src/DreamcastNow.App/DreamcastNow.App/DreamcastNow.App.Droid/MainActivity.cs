﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Gms.Common;
using System.Threading.Tasks;
using Xamarin.Forms;
using DreamcastNow.App.Views;
using DreamcastNow.App.Droid.Helpers;
using Android.Util;
using Xamarin.Forms.Platform.Android;
using Com.OneSignal;

namespace DreamcastNow.App.Droid
{
    [Activity(Label = "Dreamcast Now", Icon = "@drawable/icon", Theme = "@style/MyTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            //SetContentView(Resource.Layout.Main);
            //var toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            ////Toolbar will now take on default Action Bar characteristics
            //SetActionBar(toolbar);

            MessagingCenter.Subscribe<SettingsView, bool>(this, "NotificationSubscriptionChanged", (sender, arg) =>
            {
                NotificationHelper.ResetToken(arg);
            });

            IsPlayServicesAvailable();
            base.OnCreate(bundle);
            Forms.Init(this, bundle);
            AppDomain.CurrentDomain.UnhandledException += CurrentDomainOnUnhandledException;
            TaskScheduler.UnobservedTaskException += TaskSchedulerOnUnobservedTaskException;
            LoadApplication(new App());
        }

        private void TaskSchedulerOnUnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void CurrentDomainOnUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            throw new NotImplementedException();
        }

        public void IsPlayServicesAvailable()
        {
            int resultCode = GoogleApiAvailability.Instance.IsGooglePlayServicesAvailable(this);
            if (resultCode != ConnectionResult.Success)
            {
                DreamcastNow.App.Helpers.Settings.NotificationsAvailableSetting = false;
            }
            else
            {
                DreamcastNow.App.Helpers.Settings.NotificationsAvailableSetting = true;
                OneSignal.Current.StartInit("ab8b19a0-4351-458d-ade9-e7407dca509f").EndInit();
            }
        }
    }
}

