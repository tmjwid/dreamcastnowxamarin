using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Util;
using Firebase.Iid;
using DreamcastNow.App.Client;
using System.Threading.Tasks;
using Firebase.Messaging;

namespace DreamcastNow.App.Droid.Helpers
{
    [Service]
    [IntentFilter(new[] { "com.google.firebase.INSTANCE_ID_EVENT" })]
    class FirebaseIDService : FirebaseInstanceIdService
    {
        const string TAG = "MyFirebaseIIDService";
        public async override void OnTokenRefresh()
        {
            var refreshedToken = FirebaseInstanceId.Instance.Token;
            Log.Debug(TAG, "Refreshed token: " + refreshedToken);
            await SendRegistrationToServer(refreshedToken);
        }
        async Task SendRegistrationToServer(string token)
        {
            await new NotificationClient().SendNotificationToken(token);
        }

        public async void ResetToken()
        {
            await Task.Run(() =>
            {
                var id = FirebaseInstanceId.Instance.GetToken("732033572654", FirebaseMessaging.InstanceIdScope);
                SendRegistrationToServer(id);
            });
        }
    }
}