using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading.Tasks;
using DreamcastNow.App.Client;
using Com.OneSignal;

namespace DreamcastNow.App.Droid.Helpers
{
    public static class NotificationHelper
    {
        static bool notificationEnabled = false;
        static string oldToken;
        public static async void ResetToken(bool enabled)
        {
            await Task.Run(() =>
            {
                notificationEnabled = enabled;
                oldToken = DreamcastNow.App.Helpers.Settings.NotificationsTokenSetting;
                OneSignal.Current.IdsAvailable(IdsAvaiable);
            });
        }
        private static async void IdsAvaiable(string userID, string pushToken)
        {
            DreamcastNow.App.Helpers.Settings.NotificationsTokenSetting = userID;
            NotificationClient.SendNotificationTokenStatic(userID, notificationEnabled, oldToken);
        }
    }
}