﻿#if __ANDROID__
using DreamcastNow.Droid.Helpers;
#elif __IOS__
using DreamcastNow.iOS.Helpers;
#elif WINDOWS_UWP
using DreamcastNow.UWP.Helpers;
#endif
using DreamcastNow.Helpers;
using DreamcastNow.Interfaces;
using DreamcastNow.Services;
using DreamcastNow.Model;

namespace DreamcastNow
{
    public partial class App 
    {
        public App()
        {
        }

        public static void Initialize()
        {
            ServiceLocator.Instance.Register<IDataStore<Item>, MockDataStore>();
            ServiceLocator.Instance.Register<IMessageDialog, MessageDialog>();
            ServiceLocator.Instance.Register<IDataStore<Item>, MockDataStore>();
        }
    }
}