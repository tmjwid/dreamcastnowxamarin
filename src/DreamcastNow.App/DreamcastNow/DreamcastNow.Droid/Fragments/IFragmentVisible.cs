namespace DreamcastNow.Droid.Fragments
{
    interface IFragmentVisible
    {
        void BecameVisible();
    }
}